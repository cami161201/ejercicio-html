console.log("hola");

const contactForm = document.querySelector('.contact-form');
let nombre = document.getElementById('nombre');
let email= document.getElementById('email');
let asunto = document.getElementById('asunto');
let mensaje = document.getElementById('mensaje');

contactForm.addEventListener('submit',(e)=>{
  e.preventDefault();

  let formData = {
    nombre:nombre.value,
    email:email.value,
    asunto: asunto.value,
    mensaje: mensaje.value
  }

  console.log(formData);

  let xhr = new XMLHttpRequest();
  xhr.open('POST','/');
  xhr.setRequestHeader('content-type','application/json');
  xhr.onload = function(){
    console.log(xhr.responseText);
    if(xhr.responseText == 'success'){
      alert('Email enviado');
      nombre.value ='';
      email.value='';
      asunto.value='';
      mensaje.value='';
    }else{
      alert('Something')
    }
  }

  xhr.send(JSON.stringify(formData));
})